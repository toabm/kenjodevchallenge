import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { AlbumComponent } from './components/album/album.component';
import { AlbumAddComponent } from './components/album-add/album-add.component';
import { AlbumDetailComponent } from './components/album-detail/album-detail.component';
import { AlbumEditComponent } from './components/album-edit/album-edit.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { NavDrawerComponent } from './components/nav-bar/nav-drawer/nav-drawer.component';
import { AlbumCaseComponent } from './components/album-detail/album-case/album-case.component';
import { ArtistComponent } from './components/artist/artist.component';
import { ArtistAddComponent } from './components/artist-add/artist-add.component';
import { ArtistDetailComponent } from './components/artist-detail/artist-detail.component';
import { ArtistEditComponent } from './components/artist-edit/artist-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    AlbumComponent,
    AlbumAddComponent,
    AlbumDetailComponent,
    AlbumEditComponent,
    NavBarComponent,
    NavDrawerComponent,
    AlbumCaseComponent,
    ArtistComponent,
    ArtistAddComponent,
    ArtistDetailComponent,
    ArtistEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
