import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Add API components
import { AlbumComponent } from './components/album/album.component';
import { AlbumDetailComponent } from './components/album-detail/album-detail.component';
import { AlbumAddComponent } from './components/album-add/album-add.component';
import { AlbumEditComponent } from './components/album-edit/album-edit.component';
import {ArtistComponent} from "./components/artist/artist.component";
import {ArtistDetailComponent} from "./components/artist-detail/artist-detail.component";
import {ArtistAddComponent} from "./components/artist-add/artist-add.component";
import {ArtistEditComponent} from "./components/artist-edit/artist-edit.component";

const routes: Routes = [
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // ALBUMS ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  {
    path: 'albums',
    component: AlbumComponent,
    data: { title: 'Album List' }
  },
  {
    path: 'album-details/:id',
    component: AlbumDetailComponent,
    data: { title: 'Album Details' }
  },
  {
    path: 'album-add',
    component: AlbumAddComponent,
    data: { title: 'Album Add' }
  },
  {
    path: 'album-edit/:id',
    component: AlbumEditComponent,
    data: { title: 'Album Edit' }
  },
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // ARTIST ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  {
    path: 'artists',
    component: ArtistComponent,
    data: { title: 'Artist List' }
  },
  {
    path: 'artist-details/:id',
    component: ArtistDetailComponent,
    data: { title: 'Artist Details' }
  },
  {
    path: 'artist-add',
    component: ArtistAddComponent,
    data: { title: 'Artist Add' }
  },
  {
    path: 'artist-edit/:id',
    component: ArtistEditComponent,
    data: { title: 'Artist Edit' }
  },
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // DEFAULT ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  { path: '',
    redirectTo: '/albums',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
