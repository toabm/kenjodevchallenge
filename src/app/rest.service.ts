// https://www.djamware.com/post/5b87894280aca74669894414/angular-httpclient-678910-consume-rest-api-example

import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

/** Interface Album: Typed data for Album object */
export interface Album {
  _id: string;
  title: string;
  artistId: string;
  coverUrl: string;
  year: number;
  genre: string;
  _createdAt: Date;
  _updatedAt: Date;
}

/** Interface Artist: Typed data for Album object */
export interface Artist {
  _id: string;
  name: string;
  photoUrl: string;
  birthdate: Date;
  deathDate: Date;
  _createdAt: Date;
  _updatedAt: Date;
}

/** Our API URL */
const endpoint = 'http://localhost:3000/';

@Injectable({providedIn: 'root'})
export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response): any {
    const body = res;
    return body || { };
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // API METHODS ///////////////////////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////////////////////
  // ALBUMS REST API ///////////////////////////////////////////////////////////////////////////

  /**
   * Method to get all Album list from server.
   */
  getAlbums(): Observable<any> {
    return this.http.get<Album>(endpoint + 'albums/all').pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param id
   */
  getAlbum(id: string): Observable<any> {
    return this.http.get<Album>(endpoint + 'album/' + id).pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param album
   */
  addAlbum(album: any): Observable<any> {
    // @ts-ignore
    if (album.artistId == "") delete album.artistId;
    return this.http.post(endpoint + 'album', album).pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param id
   * @param album
   */
  updateAlbum(id: string, album: Album): Observable<any> {
    console.log(album)
    // @ts-ignore
    if (album.artistId == "") delete album.artistId;
    return this.http.put<Album>(endpoint + 'album/' + id, album).pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param id
   */
  deleteAlbum(id: string): Observable<any> {
    return this.http.delete<Album>(endpoint + 'album/' + id).pipe(
      catchError(this.handleError)
    );
  }


  //////////////////////////////////////////////////////////////////////////////////////////////
  // ARTIST REST API ///////////////////////////////////////////////////////////////////////////
  /**
   * Method to get all Album list from server.
   */
  getArtists(): Observable<any> {
    return this.http.get<Album>(endpoint + 'artists/all').pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param id
   */
  getArtist(id: string): Observable<any> {
    return this.http.get<Album>(endpoint + 'artist/' + id).pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param album
   */
  addArtist(album: any): Observable<any> {
    return this.http.post(endpoint + 'artist', album).pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param id
   * @param album
   */
  updateArtist(id: string, album: Album): Observable<any> {
    return this.http.put<Album>(endpoint + 'artist/' + id, album).pipe(
      catchError(this.handleError)
    );
  }

  /**
   *
   * @param id
   */
  deleteArtist(id: string): Observable<any> {
    return this.http.delete<Album>(endpoint + 'artist/' + id).pipe(
      catchError(this.handleError)
    );
  }





  /**
   * Method to handle errors
   * @param error
   * @private
   */
  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.statusText}, body was ==>`)
      console.error(error.error);
    }
    return throwError('Something bad happened; please try again later.')}
}
