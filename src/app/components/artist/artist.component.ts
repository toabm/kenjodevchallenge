import { Component, OnInit } from '@angular/core';
import {RestService, Artist} from '../../rest.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {

  artists: Artist[] = [];

  constructor(
    public rest: RestService,
    private router: Router) { }

  ngOnInit(): void {
    this.getArtists();
  }

  getArtists(): void {
    this.rest.getArtists().subscribe((resp: any) => {
      this.artists = resp;
    });
  }

  add(): void {
    this.router.navigate(['/artist-add']);
  }

  delete(id: string): void {
    this.rest.deleteArtist(id)
      .subscribe(() => {
          this.getArtists();
        }, (err) => {
          console.log(err);
        }
      );
  }

}
