import {Component, Input, OnInit} from '@angular/core';
import {RestService} from "../../rest.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-artist-edit',
  templateUrl: './artist-edit.component.html',
  styleUrls: ['./artist-edit.component.scss']
})
export class ArtistEditComponent implements OnInit {

  @Input() artistData: any = { name: '', photoUrl: '', birthdate: 0, deathDate: 0 };

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.rest.getArtist(this.route.snapshot.params.id).subscribe((data: {}) => {
      console.log(data);
      this.artistData = data;
    });
  }

  updateArtist(): void {
    this.rest.updateArtist(this.route.snapshot.params.id, this.artistData).subscribe((result) => {
      this.router.navigate(['/artist-details/' + result._id]);
    }, (err) => {
      console.log(err);
    });
  }

}
