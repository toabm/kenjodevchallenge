import { Component, OnInit } from '@angular/core';
import { RestService, Album } from '../../rest.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

  /** Entire list of alums from API Server. */
  albums: Album[] = [];

  /** This is a query param informing if Covers view mode must be pre-set. */
  coverView: boolean = false;

  /**
   * Class Constructor.
   *
   * @param rest            Bind our API REST Server
   * @param router          Bind our router service so we can use RouterModule .navigate() from app-routing.module.ts
   * @param activatedRoute  Bind ActivatedRoute to get Query Params from URL
   */
  constructor(
    public rest: RestService,
    private router: Router,  private activatedRoute: ActivatedRoute) { }

  /**
   * OnInit get list of albums from API and check "coverView" query param on URL
   */
  ngOnInit(): void {
    this.getAlbums(); // Get list of albums
    this.activatedRoute.queryParams.subscribe(params => { // Get "coverView" query param on URL
      this.coverView = Boolean(params['coverView']) || false;
    });
  }

  /**
   * Use API REST Service to get a list of stores albums.
   */
  getAlbums(): void {
    this.rest.getAlbums().subscribe((resp: any) => {
      this.albums = resp;});
  }

  /**
   * Use AppRoutingModule to navigate to '/album-add' component.
   */
  add(): void {this.router.navigate(['/album-add'])}

  /**
   *  Use API REST Service to delete the album for the corresponding ID.
   *
   * @param id
   */
  delete(id: string): void {
    this.rest.deleteAlbum(id)
      .subscribe(() => {this.getAlbums()}, (err) => {console.log(err)});
  }

  /**
   * Changes between normal view of albums names list, and covers view showing covers by adding/removing
   * "big" and "small" classes to .album DOM element.
   */
  toggleCoverViewMode() {
    let albums = document.getElementsByClassName("album");
    // @ts-ignore
    for (let item of albums) {
      if ( (" " + item.className + " ").replace(/[\n\t]/g, " ").indexOf(" big ") > -1 ) {
        item.classList.replace("big", "small");
      } else item.classList.replace("small", "big");
    }
  }

}
