import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {RestService, Album, Artist} from '../../rest.service'; // Import Album Interface.
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AlbumDetailComponent implements OnInit {

  // Var to store the album itself and the artist name.
  album: any;

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  /**
   * Get Album list from server onInit. It will also get corresponding artist if the album has a artistId property.
   */
  ngOnInit(): void {
    this.rest.getAlbum(this.route.snapshot.params.id).subscribe(
      (data: Album) => this.album = { ...data },
      msg => console.log('Error Getting Artist: ', msg),
      // On complete, get artistName from artistId requesting server.
      () => {
        if(this.album.hasOwnProperty("artistId") && this.album.artistId != "")
          this.rest.getArtist(this.album.artistId).subscribe(
          (data: Artist) => this.album.artistName = data.name)
      });


  }

}
