import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumCaseComponent } from './album-case.component';

describe('AlbumCaseComponent', () => {
  let component: AlbumCaseComponent;
  let fixture: ComponentFixture<AlbumCaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumCaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
