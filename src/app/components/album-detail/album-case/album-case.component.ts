import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-album-case',
  templateUrl: './album-case.component.html',
  styleUrls: ['./album-case.component.scss'],
})
export class AlbumCaseComponent implements OnInit {

  @Input() coverUrl: string;

  constructor() { this.coverUrl = ''; }

  ngOnInit(): void {
  }

}
