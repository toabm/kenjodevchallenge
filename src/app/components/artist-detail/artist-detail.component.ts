import { Component, OnInit } from '@angular/core';
import {Artist, RestService} from "../../rest.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.scss']
})
export class ArtistDetailComponent implements OnInit {

  artist: Artist | undefined;

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  /**
   * Get Album list from server onInit.
   */
  ngOnInit(): void {
    this.rest.getArtist(this.route.snapshot.params.id).subscribe(
      (data: Artist) => this.artist = { ...data });
  }

}
