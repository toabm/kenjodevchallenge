import { Component, OnInit, Input } from '@angular/core';
import {Album, Artist, RestService} from '../../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-album-edit',
  templateUrl: './album-edit.component.html',
  styleUrls: ['./album-edit.component.scss']
})
export class AlbumEditComponent implements OnInit {

  @Input() albumData: any = { title: '', coverUrl: '', year: 0, genre: '', artistId: undefined, _id: '', _createdAt: new Date(), _updatedAt: new Date() };

  artistsList: Artist[] = [];

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

    // Get correspondig Album by ID to show it details.
    this.rest.getAlbum(this.route.snapshot.params.id).subscribe((data: {}) => {
      console.log(data);
      this.albumData = data;
    });

    // Get list of Artists
    this.rest.getArtists().subscribe((data: Artist[]) => this.artistsList = data );

  }

  updateAlbum(): void {
    this.rest.updateAlbum(this.route.snapshot.params.id, this.albumData).subscribe((result) => {
      this.router.navigate(['/album-details/' + result._id]);
    }, (err) => {
      console.log(err);
    });
  }

}
