import {Component, Input, OnInit} from '@angular/core';
import {RestService} from "../../rest.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-artist-add',
  templateUrl: './artist-add.component.html',
  styleUrls: ['./artist-add.component.scss']
})
export class ArtistAddComponent implements OnInit {

  @Input() artistData = { name: '', photoUrl: '', birthdate: 0, deathDate: 0 };

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {}

  addArtist(): void {
    this.rest.addArtist(this.artistData).subscribe((result) => {
      this.router.navigate(['/artist-details/' + result._id]);
    }, (err) => {console.log(err)});
  }
}
