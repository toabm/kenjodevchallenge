import { Component, OnInit, Input } from '@angular/core';
import {Artist, RestService} from '../../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-album-add',
  templateUrl: './album-add.component.html',
  styleUrls: ['./album-add.component.scss']
})
export class AlbumAddComponent implements OnInit {

  @Input() albumData = { title: '', coverUrl: '', year: 0, genre: '', artistId: '' };
  artistsList: Artist[] = [];
  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    // Get list of Artists
    this.rest.getArtists().subscribe((data: Artist[]) => this.artistsList = data );
  }

  addAlbum(): void {
    this.rest.addAlbum(this.albumData).subscribe((result) => {
      this.router.navigate(['/album-details/' + result._id]);
    }, (err) => {console.log(err)});
  }

}
